###################################################################################################3
## builder stage
###################################################################################################3
FROM    maven:3-jdk-11-slim AS builder
COPY    src /home/app/src
COPY    pom.xml /home/app

RUN     mvn -f /home/app/pom.xml clean package

###################################################################################################3
## built image
###################################################################################################3
FROM    openjdk:11-jre-slim

COPY    --from=builder /home/app/target/hello-service-0.0.1-SNAPSHOT.jar app.jar

EXPOSE  8080

ENTRYPOINT ["java", "-jar", "app.jar"]
